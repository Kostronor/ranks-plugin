package Me.Kostronor.Ranks.Files;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import Me.Kostronor.Ranks.Ranks;

public class MySQL implements Database {

	private Connection cn = null;
	private static long timecheck = 0;
	private final Ranks plugin;
	private ResultSet r;

	public MySQL(final Ranks plugin) {
		this.plugin = plugin;
		timecheck = System.currentTimeMillis();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			cn = DriverManager.getConnection("jdbc:mysql://" + plugin.getConfig().getString("MySQL.MySQL-Hostname", "db") + "/" + plugin.getConfig().getString("MySQL.MySQL-Database", "db"), plugin
					.getConfig().getString("MySQL.MySQL-User", "user"), plugin.getConfig().getString("MySQL.MySQL-Password", "pw"));
			checkDatabase();
		} catch (Exception ex) {
			plugin.getLogger().warning(ex.getLocalizedMessage());
			ex.printStackTrace();
		}
	}

	public void reconnect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			cn = DriverManager.getConnection("jdbc:mysql://" + plugin.getConfig().getString("MySQL.MySQL-Hostname", "db") + "/" + plugin.getConfig().getString("MySQL.MySQL-Database", "db"), plugin
					.getConfig().getString("MySQL.MySQL-User", "user"), plugin.getConfig().getString("MySQL.MySQL-Password", "pw"));
			checkDatabase();

		} catch (Exception ex) {
			plugin.getLogger().warning(ex.getLocalizedMessage());
			ex.printStackTrace();
		}
	}

	public void checkConnection() throws SQLException {
		if (timeCheck()) {
			reconnect();
		}
	}

	public static boolean timeCheck() {
		if ((System.currentTimeMillis() - 100000) > timecheck) {
			timecheck = System.currentTimeMillis();
			return true;
		}
		timecheck = System.currentTimeMillis();
		return false;
	}

	public void checkDatabase() throws SQLException {
		checkConnection();
		Statement st = null;
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_users` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`Score` INT(10) NOT NULL DEFAULT '0',"
				+ "`break` INT(10) NOT NULL DEFAULT '0'," + "`place` INT(10) NOT NULL DEFAULT '0'," + "PRIMARY KEY (`Username`)" + ")");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_cache` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`Material` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',"
				+ "`break` INT(10) NOT NULL DEFAULT '0'," + "`place` INT(10) NOT NULL DEFAULT '0'," + "PRIMARY KEY (`Username`, `Material`)" + ")" + "ENGINE=MyISAM;");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_kills` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`EntityType` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',"
				+ "`kills` INT(10) NOT NULL DEFAULT '0' " + " ,PRIMARY KEY (`Username`, `EntityType`)" + ")" + "ENGINE=MyISAM;");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_damage` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`EntityType` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',"
				+ "`damage` DOUBLE NOT NULL DEFAULT '0' " + " ,PRIMARY KEY (`Username`, `EntityType`)" + ")" + "ENGINE=MyISAM;");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_online` (" + "`Username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL," + "`minutes` int(11) NOT NULL DEFAULT '0',"
				+ "PRIMARY KEY (`Username`)" + ");");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_exp` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`exp` INT(11) NOT NULL DEFAULT '0'," + "PRIMARY KEY (`Username`)"
				+ ");");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_distance` (" + "`Username` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci'," + "`distance` DOUBLE NOT NULL DEFAULT '0',"
				+ "PRIMARY KEY (`Username`)" + ")");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS `Ranks_signs` (" + " `ID` INT(11) NOT NULL AUTO_INCREMENT," + " `world` VARCHAR(255) NULL DEFAULT NULL," + " `x` INT(11) NULL DEFAULT NULL,"
				+ " `y` INT(11) NULL DEFAULT NULL," + " `z` INT(11) NULL DEFAULT NULL," + " `user` VARCHAR(15) NULL DEFAULT NULL," + " `line1` VARCHAR(15) NULL DEFAULT NULL,"
				+ " `line2` VARCHAR(15) NULL DEFAULT NULL," + " `line3` VARCHAR(15) NULL DEFAULT NULL," + " PRIMARY KEY (`ID`)," + " UNIQUE INDEX `world_x_y_z` (`world`, `x`, `y`, `z`)" + " )");

	}

	@Override
	public ResultSet showDbTable(final String table) throws SQLException {
		checkConnection();
		Statement st = null;
		ResultSet rs = null;

		st = cn.createStatement();
		rs = st.executeQuery("select * from " + table);

		return rs;
	}

	public ResultSet executeWHERE(final String table, final String statement) throws SQLException {
		checkConnection();
		Statement st = null;
		ResultSet rs = null;

		st = cn.createStatement();
		rs = st.executeQuery("select * from " + table + " WHERE " + statement);

		return rs;
	}

	@Override
	public String[][] getTopTen() {

		try {
			checkConnection();
			Statement st = null;

			st = cn.createStatement();
			r = st.executeQuery("select * from Ranks_users ORDER BY Score DESC LIMIT 10;");
			String[][] topten = new String[10][];
			int i = 0;
			while (r.next()) {
				String[] player = new String[4];
				player[0] = r.getString("Username");
				player[1] = Integer.toString(r.getInt("Score"));
				player[2] = Integer.toString(r.getInt("break"));
				player[3] = Integer.toString(r.getInt("place"));
				topten[i] = player;
				i++;
			}
			return topten;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int[] getPlayerScore(final String playername) {

		try {
			r = this.executeWHERE("Ranks_users", "Username='" + playername + "'");
			if (r.first()) {
				int[] score = new int[3];
				score[0] = r.getInt("Score");
				score[1] = r.getInt("break");
				score[2] = r.getInt("place");
				return score;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new int[] { 0, 0, 0 };
	}

	@Override
	public void updatePlayerScore(final String playername, final int score, final int breaked, final int placed) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_users(Username, Score, break, place) " + "VALUES ('" + playername + "', " + score + ", " + breaked + ", " + placed + ")"
					+ "ON DUPLICATE KEY UPDATE Score = " + score + ", break = " + breaked + ", place = " + placed + " ;");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addMinute(final String playername) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_online(Username, minutes) " + "VALUES ('" + playername + "', 1)" + "ON DUPLICATE KEY UPDATE `minutes` = `minutes` + 1;");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addExp(final String playername, final int exp) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_exp(Username, exp) " + "VALUES ('" + playername + "', " + exp + ")" + "ON DUPLICATE KEY UPDATE `exp` = `exp` + " + exp + ";");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addDistance(final String playername, final double distance) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_distance(Username, distance) " + "VALUES ('" + playername + "', " + distance + ")" + "ON DUPLICATE KEY UPDATE `distance` = `distance` + " + distance
					+ ";");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updatePlayerBreak(final String playername, final Material material) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_cache(Username, Material, break, place) " + "VALUES ('" + playername + "', '" + material.name() + "',1,0)"
					+ "ON DUPLICATE KEY UPDATE `break` = `break` + 1");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updatePlayerPlace(final String playername, final Material material) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_cache(Username, Material, break, place) " + "VALUES ('" + playername + "', '" + material.name() + "',0,1)"
					+ "ON DUPLICATE KEY UPDATE `place` = `place` + 1");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateEntityKill(final String playername, final EntityType entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_kills(Username, EntityType, kills) " + "VALUES ('" + playername + "', '" + entitytype.name() + "',1)" + "ON DUPLICATE KEY UPDATE `kills` = `kills` + 1");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateEntityDamage(final String playername, final EntityType entitytype, final double damage) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_damage(Username, EntityType, damage) " + "VALUES ('" + playername + "', '" + entitytype.name() + "'," + damage + ")"
					+ "ON DUPLICATE KEY UPDATE `damage` = `damage` + " + damage);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteEntityKill(final String playername, final String entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_kills` WHERE `Username`='" + playername + "' AND `EntityType`='" + entitytype + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteEntityDamage(final String playername, final String entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_damage` WHERE `Username`='" + playername + "' AND `EntityType`='" + entitytype + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deletePlayerCache(final String playername, final String material) {

		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_cache` WHERE `Username`='" + playername + "' AND `Material`='" + material + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deletePlayerOnline(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_online` WHERE `Username`='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deletePlayerExp(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_exp` WHERE `Username`='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deletePlayerDistance(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_distance` WHERE `Username`='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addSign(final String world, final int x, final int y, final int z, final String name, final String line1, final String line2, final String line3) {
		try {
			checkConnection();
			Statement st = null;
			st = cn.createStatement();
			st.executeUpdate("INSERT INTO `Ranks_signs` (`world`, `x`, `y`, `z`, `user`, `line1`, `line2`, `line3`) VALUES ('" + world + "', " + x + ", " + y + ", " + z + ", '" + name + "', '"
					+ line1 + "', '" + line2 + "', '" + line3 + "');");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteSign(final String world, final int x, final int y, final int z) {

		try {
			checkConnection();
			Statement st = null;
			st = cn.createStatement();
			st.executeUpdate("DELETE FROM `Ranks_signs` WHERE  `world`='" + world + "' and `x`=" + x + " and `y`= " + y + " and `z`= " + z);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disconnect() {
	}

}
