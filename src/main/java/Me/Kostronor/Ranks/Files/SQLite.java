package Me.Kostronor.Ranks.Files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import Me.Kostronor.Ranks.Ranks;

public class SQLite implements Database {

	private Connection cn = null;
	private static long timecheck = 0;
	private final Ranks plugin;
	private ResultSet r;
	boolean shutdown = false;

	public SQLite(final Ranks plugin) {
		this.plugin = plugin;
		timecheck = System.currentTimeMillis();
		try {
			Class.forName("org.sqlite.JDBC");
			this.cn = DriverManager.getConnection("jdbc:sqlite://" + plugin.getDataFolder().getAbsolutePath() + "/ranks.db");
			checkDatabase();
		} catch (Exception ex) {
			plugin.getLogger().log(Level.WARNING, "Connection to SQLite was faulty, did your server crashed?");
			// plugin.getLogger().warning(ex.getLocalizedMessage());
			// ex.printStackTrace();
			reconnect();
		}
	}

	@Override
	public void disconnect() {
		try {
			this.shutdown = true;
			this.cn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void copyFile(final File sourceFile, final File destFile) {
		try {
			if (!destFile.exists()) {
				destFile.createNewFile();
			}

			FileChannel source = null;
			FileChannel destination = null;

			try {
				source = new FileInputStream(sourceFile).getChannel();
				destination = new FileOutputStream(destFile).getChannel();
				destination.transferFrom(source, 0, source.size());
			} finally {
				if (source != null) {
					source.close();
				}
				if (destination != null) {
					destination.close();
				}
			}
		} catch (Exception e) {
		}
	}

	public boolean error = false;

	public void reconnect() {
		if (!this.shutdown) {
			try {
				Class.forName("org.sqlite.JDBC");
				this.cn = DriverManager.getConnection("jdbc:sqlite://" + this.plugin.getDataFolder().getAbsolutePath() + "/ranks.db");
				checkDatabase();

			} catch (Exception ex) {
				if (!error) {
					plugin.getLogger().log(Level.SEVERE, "Trying to restore functionality by deleting database, all signs are lost! Backup was created!");
					File f = new File(plugin.getDataFolder().getAbsolutePath() + "/ranks.db");
					File f2 = new File(plugin.getDataFolder().getAbsolutePath() + "/ranks.db.backup");
					copyFile(f, f2);
					f.delete();

					error = true;
					reconnect();
				} else {
					this.plugin.getLogger().warning(ex.getLocalizedMessage());
					ex.printStackTrace();
				}
			}
		}
	}

	public void checkConnection() throws SQLException {
		if (timeCheck()) {
			reconnect();
		}
	}

	public static boolean timeCheck() {
		if ((System.currentTimeMillis() - 100000) > timecheck) {
			timecheck = System.currentTimeMillis();
			return true;
		}
		timecheck = System.currentTimeMillis();
		return false;
	}

	public void checkDatabase() throws SQLException {
		checkConnection();
		Statement st = null;
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_users (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "Score INT(10) NOT NULL DEFAULT 0," + "break INT(10) NOT NULL DEFAULT 0,"
				+ "place INT(10) NOT NULL DEFAULT 0," + "PRIMARY KEY (Username)" + ")");
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_cache (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "Material CHAR(50) NOT NULL," + "break INT(10) NOT NULL DEFAULT 0,"
				+ "place INT(10) NOT NULL DEFAULT 0," + "PRIMARY KEY (Username, Material)" + ")");
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_kills (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "EntityType CHAR(50) NOT NULL," + "kills INT(10) NOT NULL DEFAULT 0,"
				+ "PRIMARY KEY (Username, EntityType)" + ")");
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_damage (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "EntityType CHAR(50) NOT NULL," + "damage DOUBLE NOT NULL DEFAULT 0,"
				+ "PRIMARY KEY (Username, EntityType)" + ")");
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_online (" + "Username CHAR(50) COLLATE nocase NOT NULL," + "minutes int(11) NOT NULL DEFAULT 0," + "PRIMARY KEY (Username)" + ");");
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_exp (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "exp INT(11) NOT NULL DEFAULT 0," + "PRIMARY KEY (Username)" + ");");
		st = this.cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_distance (" + "Username CHAR(50) NOT NULL COLLATE nocase," + "distance DOUBLE NOT NULL DEFAULT 0," + "PRIMARY KEY (Username)" + ")");
		st = cn.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS Ranks_signs (" + " world CHAR(255) NULL DEFAULT NULL," + " x INT(11) NULL DEFAULT NULL," + " y INT(11) NULL DEFAULT NULL,"
				+ " z INT(11) NULL DEFAULT NULL," + " user CHAR(15) NULL DEFAULT NULL," + " line1 CHAR(15) NULL DEFAULT NULL," + " line2 CHAR(15) NULL DEFAULT NULL,"
				+ " line3 CHAR(15) NULL DEFAULT NULL" + " ); CREATE UNIQUE INDEX world_x_y_z on Ranks_signs(world, x, y, z);");

	}

	@Override
	public ResultSet showDbTable(final String table) throws SQLException {
		checkConnection();
		Statement st = null;
		ResultSet rs = null;

		st = this.cn.createStatement();
		rs = st.executeQuery("select * from " + table);

		return rs;
	}

	public ResultSet executeWHERE(final String table, final String statement) throws SQLException {
		checkConnection();
		Statement st = null;
		ResultSet rs = null;

		st = this.cn.createStatement();
		rs = st.executeQuery("select * from " + table + " WHERE " + statement);

		return rs;
	}

	@Override
	public String[][] getTopTen() {

		try {
			checkConnection();
			Statement st = null;

			st = this.cn.createStatement();
			this.r = st.executeQuery("select * from Ranks_users ORDER BY Score DESC LIMIT 10;");
			String[][] topten = new String[10][];
			int i = 0;
			while (this.r.next()) {
				String[] player = new String[4];
				player[0] = this.r.getString("Username");
				player[1] = Integer.toString(this.r.getInt("Score"));
				player[2] = Integer.toString(this.r.getInt("break"));
				player[3] = Integer.toString(this.r.getInt("place"));
				topten[i] = player;
				i++;
			}
			return topten;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int[] getPlayerScore(final String playername) {

		try {
			this.r = this.executeWHERE("Ranks_users", "Username='" + playername + "'");
			if (this.r.next()) {
				int[] score = new int[3];
				score[0] = this.r.getInt("Score");
				score[1] = this.r.getInt("break");
				score[2] = this.r.getInt("place");
				return score;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new int[] { 0, 0, 0 };
	}

	@Override
	public void updatePlayerScore(final String playername, final int score, final int breaked, final int placed) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("REPLACE INTO Ranks_users(Username, Score, break, place) " + "VALUES ('" + playername + "', " + score + ", " + breaked + ", " + placed + ")");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addMinute(final String playername) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_online(Username, minutes) " + "VALUES ('" + playername + "', 0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_online SET minutes = minutes + 1 WHERE Username = '" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addExp(final String playername, final int exp) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_exp(Username, exp) " + "VALUES ('" + playername + "', 0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_exp SET exp = exp + " + exp + " WHERE Username = '" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void addDistance(final String playername, final double distance) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_distance(Username, distance) " + "VALUES ('" + playername + "', 0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_distance SET distance = distance + " + distance + " WHERE Username = '" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updatePlayerBreak(final String playername, final Material material) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_cache(Username, Material, break, place) " + "VALUES ('" + playername + "', '" + material.name() + "',0,0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_cache SET break = break + 1 WHERE Username = '" + playername + "' AND Material = '" + material.name() + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updatePlayerPlace(final String playername, final Material material) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT or IGNORE INTO Ranks_cache(Username, Material, break, place) " + "VALUES ('" + playername + "', '" + material.name() + "',0,0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_cache SET place = place + 1 WHERE Username = '" + playername + "' AND Material = '" + material.name() + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateEntityKill(final String playername, final EntityType entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_kills(Username, EntityType, kills) " + "VALUES ('" + playername + "', '" + entitytype.name() + "',0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_kills SET kills = kills + 1 WHERE Username = '" + playername + "' AND EntityType = '" + entitytype.name() + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateEntityDamage(final String playername, final EntityType entitytype, final double damage) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("INSERT OR IGNORE INTO Ranks_damage(Username, EntityType, damage) " + "VALUES ('" + playername + "', '" + entitytype.name() + "',0)");
			st = this.cn.createStatement();
			st.executeUpdate("UPDATE Ranks_damage SET damage = damage + " + damage + " WHERE Username = '" + playername + "' AND EntityType = '" + entitytype.name() + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deletePlayerCache(final String playername, final String material) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_cache WHERE Username='" + playername + "' AND Material='" + material + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteEntityKill(final String playername, final String entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_kills WHERE Username='" + playername + "' AND EntityType='" + entitytype + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteEntityDamage(final String playername, final String entitytype) {

		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_damage WHERE Username='" + playername + "' AND EntityType='" + entitytype + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deletePlayerOnline(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_online WHERE Username='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deletePlayerExp(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_exp WHERE Username='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deletePlayerDistance(final String playername) {
		try {
			checkConnection();

			Statement st = null;

			st = this.cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_distance WHERE Username='" + playername + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addSign(final String world, final int x, final int y, final int z, final String name, final String line1, final String line2, final String line3) {
		try {
			checkConnection();
			Statement st = null;
			st = cn.createStatement();
			st.executeUpdate("INSERT INTO Ranks_signs (world, x, y, z, user, line1, line2, line3) VALUES ('" + world + "', " + x + ", " + y + ", " + z + ", '" + name + "', '" + line1 + "', '" + line2
					+ "', '" + line3 + "');");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteSign(final String world, final int x, final int y, final int z) {

		try {
			checkConnection();
			Statement st = null;
			st = cn.createStatement();
			st.executeUpdate("DELETE FROM Ranks_signs WHERE  world='" + world + "' and x=" + x + " and y= " + y + " and z= " + z);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
