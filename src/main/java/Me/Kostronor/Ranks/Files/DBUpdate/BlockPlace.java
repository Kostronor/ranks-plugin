package Me.Kostronor.Ranks.Files.DBUpdate;

import org.bukkit.Material;

import Me.Kostronor.Ranks.Ranks;

public class BlockPlace implements DBUpdate {

	private final String playername;
	private final Material material;
	private boolean executed = false;

	public BlockPlace(final String playername, final Material material) {
		this.playername = playername;
		this.material = material;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().updatePlayerPlace(playername, material);
			executed = true;
		}
	}
}
