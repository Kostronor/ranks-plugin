package Me.Kostronor.Ranks.Files.DBUpdate;

import org.bukkit.entity.EntityType;

import Me.Kostronor.Ranks.Ranks;

public class EntityDamage implements DBUpdate {

	private final String playername;
	private final EntityType entitytype;
	private boolean executed = false;
	private final double damage;

	public EntityDamage(final String playername, final EntityType entitytype, final double damage) {
		this.playername = playername;
		this.entitytype = entitytype;
		this.damage = damage;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().updateEntityDamage(playername, entitytype, damage);
			executed = true;
		}
	}

}
