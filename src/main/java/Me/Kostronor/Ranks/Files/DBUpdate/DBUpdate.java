package Me.Kostronor.Ranks.Files.DBUpdate;

import Me.Kostronor.Ranks.Ranks;

public interface DBUpdate {

	void execute(Ranks plugin);

}
