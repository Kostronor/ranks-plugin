package Me.Kostronor.Ranks.Files.DBUpdate;

import Me.Kostronor.Ranks.Ranks;

public class ExpUpdate implements DBUpdate {

	private final String playername;
	private final int exp;
	private boolean executed = false;

	public ExpUpdate(final String playername, final int exp) {
		this.playername = playername;
		this.exp = exp;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().addExp(playername, exp);
			executed = true;
		}
	}

}
