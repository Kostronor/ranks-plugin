package Me.Kostronor.Ranks.Files.DBUpdate;

import Me.Kostronor.Ranks.Ranks;

public class DistanceUpdate implements DBUpdate {

	private final String playername;
	private final double distance;
	private boolean executed = false;

	public DistanceUpdate(final String playername, final double distance) {
		this.playername = playername;
		this.distance = distance;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().addDistance(playername, distance);
			executed = true;
		}
	}

}
