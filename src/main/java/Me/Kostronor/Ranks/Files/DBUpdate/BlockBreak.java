package Me.Kostronor.Ranks.Files.DBUpdate;

import org.bukkit.Material;

import Me.Kostronor.Ranks.Ranks;

public class BlockBreak implements DBUpdate {

	private final String playername;
	private final Material material;
	private boolean executed = false;

	public BlockBreak(final String playername, final Material material) {
		this.playername = playername;
		this.material = material;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().updatePlayerBreak(playername, material);
			executed = true;
		}
	}

}
