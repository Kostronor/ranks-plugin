package Me.Kostronor.Ranks.Files.DBUpdate;

import org.bukkit.entity.EntityType;

import Me.Kostronor.Ranks.Ranks;

public class EntityKill implements DBUpdate {

	private final String playername;
	private final EntityType entitytype;
	private boolean executed = false;

	public EntityKill(final String playername, final EntityType entitytype) {
		this.playername = playername;
		this.entitytype = entitytype;
	}

	@Override
	public void execute(final Ranks plugin) {
		if (!executed) {
			plugin.getDB().updateEntityKill(playername, entitytype);
			executed = true;
		}
	}

}