package Me.Kostronor.Ranks.Files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

import Me.Kostronor.Ranks.Ranks;
import Me.Kostronor.Ranks.Files.DBUpdate.DBUpdate;

public class FileHandler {
	static File Text;
	static FileConfiguration textprop;
	public static PluginDescriptionFile pdfFile = null;
	static int ticker = 0;
	public long time_cache = 0;
	public long time_update = 0;
	public long time_download = 0;
	private final Ranks plugin;
	private final boolean debug;
	private final boolean verbose;
	private final LinkedList<DBUpdate> updatelist = new LinkedList<DBUpdate>();
	private final Object updatelock = new Object();

	public FileHandler(final Ranks plugin, final boolean debug, final boolean verbose) {
		this.plugin = plugin;
		this.debug = debug;
		this.verbose = verbose;
		firstRun();
		startCacheThread();
	}

	/*
	 * This thread will flush the RAM-cache to the database Synchronous methods
	 * can use addDBUpdate to push updates to the RAM-cache. They will be
	 * written to the database on a single thread to fix some bugs with SQLite
	 * on heavy loads. When idling, the thread checks every 100 milliseconds if
	 * the plugin has shutdown or the cache has filled.
	 */
	public void startCacheThread() {
		plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {

			@Override
			public void run() {
				while (!plugin.shutdown) {

					while (updatelist.size() == 0) {
						if (plugin.shutdown) {
							return;
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!updatelist.isEmpty()) {
						DBUpdate dbu = updatelist.removeFirst();
						dbu.execute(plugin);
					}

				}
			}
		}, 100);
	}

	/**
	 * Synchronous method for adding database updates!
	 * 
	 * @param dbu
	 *            the update to be executed on the database.
	 */
	public void addDBUpdate(final DBUpdate dbu) {
		updatelist.add(dbu);
	}

	/**
	 * Creating additional yml files if not existing. Loading these files.
	 */
	public void firstRun() {
		Text = new File(plugin.getDataFolder(), "locale.yml");
		if (!Text.exists()) {
			Text.getParentFile().mkdirs();
			copy(plugin.getResource("locale.yml"), Text);
		}
		textprop = new YamlConfiguration();
		loadYamls();
	}

	/**
	 * Loading locale.yml suppressing exceptions on System.err
	 */
	public void loadYamls() {
		try {
			textprop.load(Text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Copies content from Inputstrem to File.
	 * 
	 * @param in
	 *            Stream to read from
	 * @param file
	 *            File to copy to
	 */
	private void copy(final InputStream in, final File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param key
	 *            the key to the text
	 * @return the text associated with the key
	 */
	public String getTextPropKey(final String key) {
		return textprop.getString(key, "+RError, no text spezified!");
	}

	/**
	 * every 100 ticks, saveAll() is called.
	 */
	public void saveTick() {
		ticker++;
		if (ticker >= 15) {
			saveAll();
			ticker = 0;
		}
	}

	/**
	 * clears the database cache to the webserver!
	 */
	public void saveAll() {
		clearcache();
	}

	/**
	 * logs verbose output
	 * 
	 * @param text
	 *            the logged text
	 */
	public void logInfo(final String text) {
		if (verbose) {
			plugin.getLogger().info(text);
		}
	}

	/**
	 * logs debug output
	 * 
	 * @param text
	 *            the logged text
	 */
	public void logDebug(final String text) {
		if (debug) {
			plugin.getLogger().info(text);
		}
	}

	/**
	 * logs verbose output
	 * 
	 * @param logger
	 *            the logger to log to
	 * @param text
	 *            the logged text
	 */
	public void logInfo(final Logger logger, final String text) {
		if (verbose) {
			logger.info(text);
		}
	}

	/**
	 * logs debug output
	 * 
	 * @param logger
	 *            the logger to log to
	 * @param text
	 *            the logged text
	 */
	public void logDebug(final Logger logger, final String text) {
		if (debug) {
			logger.info(text);
		}
	}

	/**
	 * Clears database cache to the webserver. This methos is synchronous and
	 * starts an Async task! Most comments in logging blocks!
	 */
	public void clearcache() {
		final Logger logger = plugin.getLogger();
		logInfo(logger, "Begin data-synchronisation on " + getServer());
		plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {

			@Override
			public void run() {
				/*
				 * Synchronize database flushing, as only one thread at a time
				 * is safe doing it!
				 */
				synchronized (updatelock) {
					long starttime = System.currentTimeMillis();
					logInfo(logger, "Begin [UP] data-synchronisation...");
					try {
						logInfo(logger, "Begin [CACHE] data-synchronisation...");
						ResultSet rs = plugin.getDB().showDbTable("Ranks_cache");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | " + rs.getString("Material") + " (" + rs.getInt("break") + "|" + rs.getInt("place") + ")");
							plugin.getDB().deletePlayerCache(rs.getString("Username"), rs.getString("Material"));
							cacheUpdate(rs.getString("Username"), rs.getString("Material"), rs.getInt("break"), rs.getInt("place"));
						}

						logInfo(logger, "Begin [ONLINE] data-synchronisation...");
						rs = plugin.getDB().showDbTable("Ranks_online");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | ONLINE (" + rs.getInt("minutes") + ")");
							plugin.getDB().deletePlayerOnline(rs.getString("Username"));
							onlineUpdate(rs.getString("Username"), rs.getInt("minutes"));
						}

						logInfo(logger, "Begin [EXP] data-synchronisation...");
						rs = plugin.getDB().showDbTable("Ranks_exp");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | EXP (" + rs.getInt("exp") + ")");
							plugin.getDB().deletePlayerExp(rs.getString("Username"));
							expUpdate(rs.getString("Username"), rs.getInt("exp"));
						}

						logInfo(logger, "Begin [DISTANCE] data-synchronisation...");
						rs = plugin.getDB().showDbTable("Ranks_distance");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | DISTANCE (" + rs.getDouble("distance") + ")");
							plugin.getDB().deletePlayerDistance(rs.getString("Username"));
							distanceUpdate(rs.getString("Username"), rs.getDouble("distance"));
						}

						logInfo(logger, "Begin [DAMAGE] data-synchronisation...");
						rs = plugin.getDB().showDbTable("Ranks_damage");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | [" + rs.getString("EntityType") + "] DAMAGE (" + rs.getDouble("damage") + ")");
							plugin.getDB().deleteEntityDamage(rs.getString("Username"), rs.getString("EntityType"));
							damageUpdate(rs.getString("Username"), rs.getString("EntityType"), rs.getDouble("damage"));
						}

						logInfo(logger, "Begin [KILL] data-synchronisation...");
						rs = plugin.getDB().showDbTable("Ranks_kills");
						while (rs.next()) {
							logDebug(logger, "Uploading: " + rs.getString("Username") + " | [" + rs.getString("EntityType") + "] KILLS (" + rs.getInt("kills") + ")");
							plugin.getDB().deleteEntityKill(rs.getString("Username"), rs.getString("EntityType"));
							killUpdate(rs.getString("Username"), rs.getString("EntityType"), rs.getInt("kills"));
						}

					} catch (SQLException e) {
						e.printStackTrace();
					}
					logInfo(logger, "Finished [UP] data-synchronisation...");
					long midtime = System.currentTimeMillis() - starttime;
					logInfo(logger, "Begin [DOWN] data-synchronisation...");
					/*
					 * Updating the local highscore. Retrieving server specific
					 * data.
					 */
					String s = "http://mcranks.org/game.php?o=true&i=" + getServer();
					URLConnection connection;
					try {
						connection = new URL(s).openConnection();
						BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						while (br.ready()) {

							String line = br.readLine();
							String[] split = line.split(";");
							String name = split[0];
							int score = Integer.parseInt(split[1]);
							int breaked = Integer.parseInt(split[2]);
							int placed = Integer.parseInt(split[3]);

							plugin.getDB().updatePlayerScore(name, score, breaked, placed);

						}
						br.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					logInfo(logger, "Finished [DOWN] data-synchronisation...");
					long endtime = System.currentTimeMillis() - starttime;
					logInfo(logger, "Begin [TIME] data-synchronisation...");
					/*
					 * Upload time needed to flush cache for statistics
					 * purposes.
					 */
					time_cache = (time_cache + endtime) / 2;
					time_update = (time_cache + midtime) / 2;
					time_download = (time_cache + (endtime - midtime)) / 2;
					try {
						String server = getServer();
						if (server != null) {
							String url = "http://mcranks.org/game.php?time_cache=" + time_cache + "&time_update=" + time_update + "&time_download=" + time_download + "&i=" + server;
							URLConnection urlconnection = new URL(url).openConnection();
							urlconnection.getInputStream().close();

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					logInfo(logger, "Finished [TIME] data-synchronisation...");
					updateSigns();
				}
			}
		}, 20);

	}

	public void updateSigns() {

		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

			@Override
			public void run() {
				logInfo("Starting sign updates...");
				ResultSet rs;
				try {
					rs = plugin.getDB().showDbTable("Ranks_signs");
					while (rs.next()) {
						logDebug("Found sign : " + rs.getString("user") + " | (" + rs.getString("line1") + ") | (" + rs.getString("line2") + ") | (" + rs.getString("line3") + ")");
						World w = plugin.getServer().getWorld(rs.getString("world"));
						Block b = w.getBlockAt(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
						if (!b.getChunk().isLoaded()) {
							b.getChunk().load();
						}
						if ((b.getType() == Material.SIGN_POST) || (b.getType() == Material.WALL_SIGN)) {
							Sign s = (Sign) b.getState();
							s.setLine(0, ChatColor.YELLOW + rs.getString("user"));
							int[] score = plugin.getDB().getPlayerScore(rs.getString("user"));
							s.setLine(1, (Integer.toString(score[0])));
							s.setLine(2, (Integer.toString(score[1])));
							s.setLine(3, (Integer.toString(score[2])));
							s.update();
						} else {
							plugin.getDB().deleteSign(rs.getString("world"), rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				logInfo("Finished sign updates...");
			}
		});

	}

	/**
	 * Creates a distinct server URI used by the webserver
	 * 
	 * @return a distinct server URI
	 */
	private String getServer() {
		String ip = plugin.getServer().getIp();
		if ((ip == null) || ip.equals("")) {
			ip = "local_server";
		}
		return ip + ":" + plugin.getServer().getPort();
	}

	/**
	 * Uploads a single players broken and placed blocks of one material
	 * 
	 * @param playername
	 *            the players minecraft name
	 * @param material
	 *            the material of the blocks
	 * @param blockbreak
	 *            number of broken blocks
	 * @param blockplace
	 *            number of placed blocks
	 */
	private void cacheUpdate(final String playername, final String material, final int blockbreak, final int blockplace) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?m=" + material + "&n=" + playername + "&i=" + server + "&b=" + blockbreak + "&p=" + blockplace;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Updates a single players online minutes
	 * 
	 * @param playername
	 *            the players minecraft name
	 * @param minutes
	 *            the number of minutes online
	 */
	private void onlineUpdate(final String playername, final int minutes) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?t=" + minutes + "&n=" + playername + "&i=" + server;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates a single players gained experience
	 * 
	 * @param playername
	 *            the players minecraft name
	 * @param exp
	 *            the number of experience points gained
	 */
	private void expUpdate(final String playername, final int exp) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?e=" + exp + "&n=" + playername + "&i=" + server;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates a single players distance walked
	 * 
	 * @param playername
	 *            the players minecraft name
	 * @param distance
	 *            the distance walked
	 */
	private void distanceUpdate(final String playername, final double distance) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?d=" + distance + "&n=" + playername + "&i=" + server;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void damageUpdate(final String playername, final String string, final double damage) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?s=" + damage + "&n=" + playername + "&i=" + server;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void killUpdate(final String playername, final String mob, final int kills) {
		try {
			String server = getServer();
			if (server != null) {
				String s = "http://mcranks.org/game.php?m=" + mob + "&n=" + playername + "&i=" + server + "&k=" + kills;
				URLConnection connection = new URL(s).openConnection();
				connection.getInputStream().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
