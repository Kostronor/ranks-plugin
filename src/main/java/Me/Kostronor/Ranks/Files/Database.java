package Me.Kostronor.Ranks.Files;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

public interface Database {

	/**
	 * disconnects the database
	 */
	void disconnect();

	/**
	 * int[0] = score, int[1] = break, int[2] = place
	 * 
	 * @param name
	 *            player minecraft name
	 * @return local score in database
	 */
	int[] getPlayerScore(String name);

	/**
	 * @param name
	 *            player minecraft name
	 * @param score
	 *            updated score
	 * @param breaked
	 *            updated breaks
	 * @param placed
	 *            updated places
	 */
	void updatePlayerScore(String name, int score, int breaked, int placed);

	/**
	 * removes entry from database cache
	 * 
	 * @param playername
	 *            player minecraft name
	 * @param material
	 *            material
	 */
	void deletePlayerCache(String playername, String material);

	/**
	 * Executes select * from table 'table'
	 * 
	 * @param table
	 *            the table to be selected
	 * @return Resultset of table data
	 * @throws SQLException
	 *             on failure
	 */
	ResultSet showDbTable(String table) throws SQLException;

	void deletePlayerOnline(String string);

	void deletePlayerExp(String string);

	void deletePlayerDistance(String string);

	String[][] getTopTen();

	void addDistance(String name, double distance);

	void addExp(String name, int amount);

	void addMinute(String name);

	void updatePlayerBreak(String playername, Material material);

	void updatePlayerPlace(String playername, Material material);

	void updateEntityKill(String playername, EntityType entitytype);

	void deleteEntityKill(String playername, String entitytype);

	void updateEntityDamage(String playername, EntityType entitytype, double damage);

	void deleteEntityDamage(String playername, String entitytype);

	void addSign(String world, int x, int y, int z, String name, String line1, String line2, String line3);

	void deleteSign(String world, int x, int y, int z);

}
