package Me.Kostronor.Ranks.events;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import Me.Kostronor.Ranks.Ranks;
import Me.Kostronor.Ranks.Files.DBUpdate.DistanceUpdate;
import Me.Kostronor.Ranks.Files.DBUpdate.ExpUpdate;

public class RanksPlayerListener implements Listener {

	public static Ranks Plugin;

	public RanksPlayerListener(final Ranks instance) {
		Plugin = instance;
	}

	/*
	 * Array for examining our surrounding blocks.
	 */
	BlockFace[] faces = { BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH_EAST, BlockFace.NORTH_WEST, BlockFace.SOUTH_EAST, BlockFace.SOUTH_WEST };

	@EventHandler
	public void onPlayerMove(final PlayerMoveEvent e) {

		final double distance = e.getFrom().distance(e.getTo());
		Ranks.FH.addDBUpdate(new DistanceUpdate(e.getPlayer().getName(), distance));
		/*
		 * Create a large 3x1 block for any given direction
		 */
		Block xlocBlock = e.getPlayer().getLocation().getBlock();
		Block hlocBlock = xlocBlock.getRelative(BlockFace.UP);
		Block tlocBlock = hlocBlock.getRelative(BlockFace.UP);
		/*
		 * Loop through all directions
		 */
		for (int x = 0; x < this.faces.length; x++) {
			/*
			 * Looking for blocks up to 6 blocks away
			 */
			for (int y = 0; y < 6; y++) {
				if ((xlocBlock.getRelative(this.faces[x], y).getType() == Material.SIGN_POST) || (xlocBlock.getRelative(this.faces[x], y).getType() == Material.WALL_SIGN)) {
					editSign(xlocBlock, x, y);
				} else if ((hlocBlock.getRelative(this.faces[x], y).getType() == Material.WALL_SIGN) || (hlocBlock.getRelative(this.faces[x], y).getType() == Material.SIGN_POST)) {
					editSign(hlocBlock, x, y);
				} else if ((tlocBlock.getRelative(this.faces[x], y).getType() == Material.WALL_SIGN) || (tlocBlock.getRelative(this.faces[x], y).getType() == Material.SIGN_POST)) {
					editSign(tlocBlock, x, y);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerExpChange(final PlayerExpChangeEvent e) {
		Ranks.FH.addDBUpdate(new ExpUpdate(e.getPlayer().getName(), e.getAmount()));
	}

	/**
	 * update sign texts
	 */
	public void editSign(final Block locBlock, final int x, final int y) {
		BlockState sBlock = locBlock.getRelative(this.faces[x], y).getState();
		Sign sign = (Sign) sBlock;
		if (sign.getLine(0).trim().equalsIgnoreCase("[Ranks]")) {
			String name = sign.getLine(1).trim();
			int[] score = Plugin.getDB().getPlayerScore(name);
			sign.setLine(2, (Integer.toString(score[1])));
			sign.setLine(3, (Integer.toString(score[2])));
			sign.update();
		}

	}

}
