package Me.Kostronor.Ranks.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;

import Me.Kostronor.Ranks.Ranks;
import Me.Kostronor.Ranks.Files.DBUpdate.BlockBreak;
import Me.Kostronor.Ranks.Files.DBUpdate.BlockPlace;

public class RanksBlockListener implements Listener {
	Ranks parent;

	public RanksBlockListener(final Ranks parent) {
		this.parent = parent;
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent e) {
		Player player = e.getPlayer();
		String name = player.getName();
		addBlockBreak(name, e.getBlock().getType());
	}

	public void addBlockBreak(final String playername, final Material material) {
		Ranks.FH.addDBUpdate(new BlockBreak(playername, material));
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent e) {
		Player player = e.getPlayer();
		String name = player.getName();
		addBlockPlace(name, e.getBlock().getType());
	}

	public void addBlockPlace(final String playername, final Material material) {
		Ranks.FH.addDBUpdate(new BlockPlace(playername, material));
	}

	@EventHandler
	public void onSignChange(final SignChangeEvent e) {
		if (e.getLine(0).trim().equalsIgnoreCase("[Ranks]")) {
			String name = e.getLine(1).trim();
			int[] score = this.parent.getDB().getPlayerScore(name);
			e.setLine(2, (Integer.toString(score[1])));
			e.setLine(3, (Integer.toString(score[2])));
		} else if (e.getLine(0).trim().startsWith("[") && e.getLine(0).trim().endsWith("]")) {
			Ranks.FH.logInfo("sign found!" + e.toString());
			parent.getDB().addSign(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(),
					e.getLine(0).trim().substring(1, e.getLine(0).trim().length() - 1), e.getLine(1).trim(), e.getLine(2).trim(), e.getLine(3).trim());
			Ranks.FH.updateSigns();
		}

	}
}
