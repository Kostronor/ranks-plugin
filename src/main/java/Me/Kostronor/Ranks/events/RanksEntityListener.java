package Me.Kostronor.Ranks.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import Me.Kostronor.Ranks.Ranks;
import Me.Kostronor.Ranks.Files.DBUpdate.EntityDamage;
import Me.Kostronor.Ranks.Files.DBUpdate.EntityKill;

public class RanksEntityListener implements Listener {

	public static Ranks Plugin;

	public RanksEntityListener(final Ranks instance) {
		Plugin = instance;
	}

	@EventHandler
	public void onEntityDeath(final EntityDeathEvent e) {
		if (e.getEntity().getKiller() != null) {
			Player p = e.getEntity().getKiller();
			Ranks.FH.addDBUpdate(new EntityKill(p.getName(), e.getEntityType()));
		}
	}

	@EventHandler
	public void onEntityDamageByEntity(final EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			double d = e.getDamage();
			Ranks.FH.addDBUpdate(new EntityDamage(p.getName(), e.getEntityType(), d));
		}
	}

}
