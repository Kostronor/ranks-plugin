package Me.Kostronor.Ranks;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import Me.Kostronor.Ranks.Files.Database;
import Me.Kostronor.Ranks.Files.FileHandler;
import Me.Kostronor.Ranks.Files.MySQL;
import Me.Kostronor.Ranks.Files.SQLite;
import Me.Kostronor.Ranks.events.RanksBlockListener;
import Me.Kostronor.Ranks.events.RanksEntityListener;
import Me.Kostronor.Ranks.events.RanksPlayerListener;
import Me.Kostronor.Ranks.metrics.Metrics;
import Me.Kostronor.Ranks.metrics.Metrics.Graph;
import Me.Kostronor.Ranks.update.Updater;

public class Ranks extends JavaPlugin {
	public static FileHandler FH = null;

	private final RanksPlayerListener playerListener = new RanksPlayerListener(this);
	private final RanksBlockListener blockListener = new RanksBlockListener(this);
	private final RanksEntityListener entityListener = new RanksEntityListener(this);

	public FileConfiguration configuration;
	public static PluginDescriptionFile pdfFile = null;
	private Database DB;
	Ranks plugin;

	/**
	 * If true, the plugin will use MySQL, if false, it will use SQLite.
	 */
	public boolean mysql;

	/**
	 * If true, the plugin has shut down and tasks have to kill themselves!
	 */
	public boolean shutdown = false;

	@Override
	public void onEnable() {
		/*
		 * Some standard plugin things Copying configuration - already created
		 * config will not be modified, as comments would be lost! Getting some
		 * infos about the plugin
		 */
		this.plugin = this;
		this.saveDefaultConfig();
		this.configuration = getConfig();
		this.configuration.options().copyDefaults(true);
		pdfFile = this.getDescription();

		/*
		 * Some checking for online mode and configuration
		 */
		if (getServer().getOnlineMode()) {
			if (this.configuration.getBoolean("enabled", true)) {
				enablePlugin();
			} else {
				getLogger().info("You have deactivated the plugin!");
			}
		} else {
			getLogger().warning("Your server is running in offline-mode, this disables Ranks!");
		}
		if (this.configuration.getBoolean("auto-update", true)) {
			new Updater(this, 54025, this.getFile(), Updater.UpdateType.DEFAULT, false);
		}
		if (this.configuration.getBoolean("enable-metrics", true)) {
			enableMetrics();
		}
	}

	/**
	 * called once by onEnable() if the plugin is enabled!
	 * 
	 */
	public void enablePlugin() {
		boolean debug = this.configuration.getBoolean("debug", false);
		boolean verbose = (this.configuration.getBoolean("verbose", false) || debug);
		FH = new FileHandler(this, debug, verbose);
		getLogger().info(pdfFile.getName() + " version " + pdfFile.getVersion() + " by Kostronor is enabled!");
		if (this.configuration.getBoolean("stats-tracking", true)) {
			enableStatstracking();
		}
	}

	/**
	 * called once by enablePlugin when statstracking is enabled!
	 */
	public void enableStatstracking() {
		PluginManager pm = this.getServer().getPluginManager();
		if (this.configuration.getBoolean("Use MySQL", false)) {
			this.DB = new MySQL(this);
			this.mysql = true;
		} else {
			this.DB = new SQLite(this);
			this.mysql = false;
		}
		pm.registerEvents(this.playerListener, this);
		pm.registerEvents(this.blockListener, this);
		pm.registerEvents(this.entityListener, this);
		FH.clearcache();
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			@Override
			public void run() {
				for (Player player : Ranks.this.plugin.getServer().getOnlinePlayers()) {
					Ranks.this.plugin.getDB().addMinute(player.getName());
					FH.saveTick();
				}
			}
		}, 20 * 60, 20 * 60);
	}

	/**
	 * called once by enablePlugin when metrics is enabled!
	 */
	public void enableMetrics() {
		final boolean usemysql = this.mysql;
		getServer().getScheduler().runTaskLater(this, new Runnable() {

			@Override
			public void run() {

				try {
					Metrics metrics = new Metrics(Ranks.this.plugin);
					String database = "SQLite";
					if (usemysql) {
						database = "MySQL";
					}
					Graph databaseGraph = metrics.createGraph("Database Engine");
					databaseGraph.addPlotter(new Metrics.Plotter(database) {
						@Override
						public int getValue() {
							return 1;
						}
					});
					Graph timeGraph = metrics.createGraph("Performance");
					timeGraph.addPlotter(new Metrics.Plotter("Cache Overall") {
						@Override
						public int getValue() {
							return (int) FH.time_cache;
						}
					});
					timeGraph.addPlotter(new Metrics.Plotter("Cache Update") {
						@Override
						public int getValue() {
							return (int) FH.time_update;
						}
					});
					timeGraph.addPlotter(new Metrics.Plotter("Cache Download") {
						@Override
						public int getValue() {
							return (int) FH.time_download;
						}
					});
					metrics.start();
				} catch (Exception e) {
					// Failed to submit the stats :-(
				}

			}
		}, 20 * 60);
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
		String[] split = args;
		final String name;
		final Server server = this.getServer();
		if (commandLabel.equalsIgnoreCase("Ranks")) {
			if (split.length == 1) {
				if (split[0].equalsIgnoreCase("topten")) {
					sender.sendMessage(InsertStrings("+YPlease wait, while the top ten list is processed!"));

					this.getServer().getScheduler().runTaskLaterAsynchronously(this, new Runnable() {

						@Override
						public void run() {
							final String[][] topten = getDB().getTopTen();

							server.getScheduler().runTask(Ranks.this.plugin, new Runnable() {

								@Override
								public void run() {
									int i = 0;
									if (topten != null) {
										while (i < topten.length) {
											if (topten[i] != null) {
												sender.sendMessage(InsertStrings(("+YRank [+G" + (i + 1) + "+Y] +ais +r" + topten[i][0] + "+a with +p" + topten[i][1] + "+a points.")));
											}
											i++;
										}

									}
								}

							});
						}
					}, 1);
					return true;
				}
				name = split[0];
			} else {
				name = sender.getName();
			}

			this.getServer().getScheduler().runTaskLaterAsynchronously(this, new Runnable() {

				@Override
				public void run() {
					final int[] score = getDB().getPlayerScore(name);

					server.getScheduler().runTask(Ranks.this.plugin, new Runnable() {

						@Override
						public void run() {
							sender.sendMessage(InsertStrings(FH.getTextPropKey("Textcommand"), name, Integer.toString(score[1]), Integer.toString(score[2])));
						}

					});
				}
			}, 1);

			return true;
		}
		return false;
	}

	/**
	 * Replaces color codes with their corresponding minecraft color. Replaces
	 * +name with the coresponding name. Replaces +break with the coresponding
	 * number. Replaces +place with the coresponding number.
	 * 
	 * @param input
	 *            the string to be formatted
	 * @param name
	 *            the name to replace +name with
	 * @param broken
	 *            the number to replace +break with
	 * @param placed
	 *            the number to replace +place with
	 * @return a color formated String
	 */
	public static String InsertStrings(final String input, final String name, final String broken, final String placed) {
		String result = "";
		result = input.replace("+break", broken);
		result = result.replace("+place", placed);
		result = InsertStrings(result, name);
		return result;

	}

	/**
	 * Replaces color codes with their corresponding minecraft color. Replaces
	 * +name with the coresponding name.
	 * 
	 * @param input
	 *            the string to be formatted
	 * @param name
	 *            the name to replace +name with
	 * @return a color formated string
	 */
	public static String InsertStrings(final String input, final String name) {
		String result = "";
		result = input.replace("+name", name);
		result = InsertStrings(result);
		return result;
	}

	/**
	 * Replaces color codes with their corresponding minecraft color.
	 * 
	 * @param input
	 *            the string to be formatted
	 * @return a color formated string
	 */
	public static String InsertStrings(final String input) {
		String result = "";
		result = input.replace("+r", ChatColor.RED.toString());
		result = result.replace("+R", ChatColor.DARK_RED.toString());
		result = result.replace("+y", ChatColor.YELLOW.toString());
		result = result.replace("+Y", ChatColor.GOLD.toString());
		result = result.replace("+g", ChatColor.GREEN.toString());
		result = result.replace("+G", ChatColor.DARK_GREEN.toString());
		result = result.replace("+a", ChatColor.AQUA.toString());
		result = result.replace("+A", ChatColor.DARK_AQUA.toString());
		result = result.replace("+b", ChatColor.BLUE.toString());
		result = result.replace("+B", ChatColor.DARK_BLUE.toString());
		result = result.replace("+p", ChatColor.LIGHT_PURPLE.toString());
		result = result.replace("+P", ChatColor.DARK_PURPLE.toString());
		result = result.replace("+k", ChatColor.BLACK.toString());
		result = result.replace("+s", ChatColor.GRAY.toString());
		result = result.replace("+S", ChatColor.DARK_GRAY.toString());
		result = result.replace("+w", ChatColor.WHITE.toString());
		return result;
	}

	@Override
	public void onDisable() {
		getLogger().info("Saving data");
		/*
		 * cancel all tasks associated with the plugin!
		 */
		this.getServer().getScheduler().cancelTasks(this);
		/*
		 * notify running tasks to shutdown!
		 */
		this.shutdown = true;
		/*
		 * disconnect database, write SQLite to file!
		 */
		this.DB.disconnect();
	}

	public FileConfiguration getConfigs() {
		return this.configuration;
	}

	public Database getDB() {
		return this.DB;
	}
}
